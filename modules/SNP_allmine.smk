__author__ = "INRAE GAFL"
__license__ = "MIT"
__copyright__ = "INRAE, GAFL 2020"


#####################################################################################
######################### allmine ###################################################
rule parse_bam_with_bed:
    input:
        bam   = "{outdir}/mapped/{{sample}}_sorted.bam".format(outdir=config["outdir"]),
        bai   = "{outdir}/mapped/{{sample}}_sorted.bam.bai".format(outdir=config["outdir"]),
        stats = "{outdir}/mapped/{{sample}}.stats.txt".format(outdir=config["outdir"])
    output:
        bam   = "{outdir}/mapped/{{sample}}_sorted_parsed.bam".format(outdir=config["outdir"]),
        bamidx   = "{outdir}/mapped/{{sample}}_sorted_parsed.bam.bai".format(outdir=config["outdir"])
    params:
        bed = config["REGIONS"],
        ref = config["REFPATH"] + "/" + config["GENOME"],
        bind = config["BIND"],
        samtools_bin = config["samtools_bin"]
    message: "Parsing {input.bam} using the blueprint {params.bed} \n"
    shell:
        """
        singularity exec {params.bind} {params.samtools_bin} samtools view \
        -b \
        -L {params.bed} \
        -O BAM \
        -o {output.bam} {input.bam}
        singularity exec {params.bind} {params.samtools_bin} samtools index {output.bam}
        """

rule varscan:
    input:
        bam = "{outdir}/mapped/{{sample}}_sorted_parsed.bam".format(outdir=config["outdir"])
    output:
        var = "{outdir}/variant/{{sample}}_varscan.vcf".format(outdir=config["outdir"])
    params:
        ref = config["REFPATH"] + "/" + config["GENOME"],
        bind = config["BIND"],
        samtools_bin = config["samtools_bin"],
        varscan_bin  = config["varscan_bin"],
        freebayes_bin = config["freebayes_bin"]
    message: "Looking for SNP in {input.bam} with Varscan \n"
    #from mpileup to varscan to save disk space
    shell:
        """
        zero=$(singularity exec {params.bind} {params.samtools_bin} samtools flagstat {input.bam}|head -n1|sed 's/\s.*//g')
        if [ "$zero" -lt "1" ]
        then
                echo "WARNING ALLMINE {output.var} EMPTY"
                touch {output.var}
                exit 0
        fi
        singularity exec {params.bind} {params.freebayes_bin} \
        freebayes -f {params.ref} \
        --min-mapping-quality 30 \
        --min-base-quality 20 \
        --min-alternate-fraction 0.15 \
        --min-coverage 8 \
        --use-duplicate-reads \
        {input.bam} > {output.var}
        exit 0

        singularity exec {params.bind} {params.samtools_bin} \
        samtools mpileup \
        -C 50 \
        -A \
        -d 50000 \
        -f {params.ref} \
        {input.bam} | \
        singularity exec {params.bind} {params.varscan_bin} \
        varscan mpileup2snp \
        --p-value 0.99 \
        --min-coverage 8 \
        --output-vcf 1 \
        --min-strands2 1 \
        --min-var-freq 0.15 \
        --min-freq-for-hom 0.75 \
        --min-avg-qual 20 \
        > {output.var}
        """

rule annovar :
    input:
        inav = "{outdir}/variant/{{sample}}_varscan.avinput".format(outdir=config["outdir"])
    output:
        all_vars ="{outdir}/variant/{{sample}}_varscan.avinput.variant_function".format(outdir=config["outdir"]),
        exonic_vars ="{outdir}/variant/{{sample}}_varscan.avinput.exonic_variant_function".format(outdir=config["outdir"])
    params:
        avdb = config["AVDB"],
        bind = config["BIND"],
        allmine_bin = config["allmine_bin"]
    message: "Annotating {input.inav} with Annovar \n"
    shell:
        """
        singularity exec {params.bind} {params.allmine_bin} \
        /opt/annovar/annotate_variation.pl \
        --geneanno \
        --dbtype refGene \
        --buildver AV \
        {input.inav} \
        {params.avdb} \
        """

rule alternative_proteins:
    input:
        exonic_vars ="{outdir}/variant/{{sample}}_varscan.avinput.exonic_variant_function".format(outdir=config["outdir"])
    output:
        prots = "{outdir}/variant/{{sample}}_mutated_proteins.fasta".format(outdir=config["outdir"])
    params:
        avdb = config["AVDB"],
        bind = config["BIND"],
        allmine_bin = config["allmine_bin"]
    message: "Writing mutated proteins from {input.exonic_vars} variants \n"
    shell:
        """
        singularity exec {params.bind} {params.allmine_bin} \
        /opt/annovar/coding_change.pl \
        --includesnp \
        --onlyAltering \
        {input.exonic_vars} \
        {params.avdb}'/'AV_refGene.txt \
        {params.avdb}'/'AV_refGeneMrna.fa \
        > {output.prots}
        """

rule make_report :
    input:
        non_syno = lambda wildcards: expand(expand("{outdir}/variant/{sample}_varscan.avinput.exonic_variant_function", outdir=config["outdir"], sample=samples['SampleName']))
    output:
        report = "{outdir}/Non_synonymous_variants_summary.tab".format(outdir=config["outdir"])
    params:
        inpath      = "{outdir}/variant".format(outdir=config["outdir"]),
        sampleslist = lambda wildcards: expand(expand("{sample}",sample=samples['SampleName'])),
        suffix      = "_varscan.avinput.exonic_variant_function",
        bind        = config["BIND"],
        allmine_bin = config["allmine_bin"]
    shell:
        """
        singularity exec {params.bind} {params.allmine_bin} \
        python3 ./modules/report_generator.py \
        {params.inpath} \
        "{params.sampleslist}" \
        {params.suffix} \
        {output.report}
        """

rule parsed_bams_list :
    input:
        bams = lambda wildcards: expand(expand("{outdir}/mapped/{sample}_sorted_parsed.bam", outdir=config["outdir"], sample=samples['SampleName'])),
        bamsidx = lambda wildcards: expand(expand("{outdir}/mapped/{sample}_sorted_parsed.bam.bai", outdir=config["outdir"], sample=samples['SampleName']))
    output:
        bamslist = "{outdir}/sorted_parsed.bams.list".format(outdir=config["outdir"])
    params:
        bamsp       = "{outdir}/mapped".format(outdir=config["outdir"]),
        outlist     = config["outdir"]
    shell:
        """
        #ls {params.bamsp}/*.bam > {params.outlist}/bams.list
        echo {input.bams}|tr -s '[:space:]' '\n' >{output.bamslist}
        """

rule cov_track:
    input:
        bamslist = "{outdir}/sorted_parsed.bams.list".format(outdir=config["outdir"])
    output:
        cov = "{outdir}/Coverage_Track.tab".format(outdir=config["outdir"])
    params:
        bed = config["REGIONS"],
        bind = config["BIND"],
        samtools_bin = config["samtools_bin"]
    message: "Computing depth in regions {params.bed} on bed parsed bams list: {input.bamslist} \n"
    shell:
        """
        singularity exec {params.bind} {params.samtools_bin} \
        samtools depth \
        -a \
        -b {params.bed} \
        -f {input.bamslist} > {output.cov}
        """

rule vcf4_to_avinput:
    input:
        vcf = "{outdir}/variant/{{sample}}_varscan.vcf".format(outdir=config["outdir"])
    output:
        annovar ="{outdir}/variant/{{sample}}_varscan.avinput".format(outdir=config["outdir"])
    params:
        bind = config["BIND"],
        allmine_bin = config["allmine_bin"]
    message: "Converting {input.vcf} to avinput format \n"
    shell:
        """
        singularity exec {params.bind} {params.allmine_bin} \
        /opt/annovar/convert2annovar.pl \
        -format vcf4 \
        -outfile {output.annovar} \
        {input.vcf}
        """

rule whatshap:
    input:
        ref = config["REFPATH"] + "/" + config["GENOME"],
        bamidx   = "{outdir}/mapped/{{sample}}_sorted_parsed.bam.bai".format(outdir=config["outdir"]),
        ref_index = config["REFPATH"] + "/" + config["GENOME"] + ".fai",
        bam = "{outdir}/mapped/{{sample}}_sorted_parsed.bam".format(outdir=config["outdir"]),
        var = "{outdir}/variant/{{sample}}_varscan.vcf".format(outdir=config["outdir"])
    output:
        phased = "{outdir}/variant/{{sample}}_varscan_phased.vcf".format(outdir=config["outdir"])
    params:
        bind = config["BIND"],
        allmine_bin = config["allmine_bin"]
    message: "Phasing variant from {input.var} using WhatsHap."
    shell:
        """
        if [ ! -s {input.var} ]
        then
                echo 'WARNING: _varscan.vcf empty'
                touch {output.phased}
                exit 0
        fi
        singularity exec {params.bind} {params.allmine_bin} \
        whatshap phase \
        -H 20 \
        --reference {input.ref} \
        -o {output.phased} \
        {input.var} \
        {input.bam}
        """

rule make_markdown:
    input:
        snps = "{outdir}/Non_synonymous_variants_summary.tab".format(outdir=config["outdir"]),
        cov  = "{outdir}/Coverage_Track.tab".format(outdir=config["outdir"])
    output:
        Rmd  = "{outdir}/Run_report.html".format(outdir=config["outdir"])
    params:
        sf          = config["samplesfile"],
        wd          = "{outdir}".format(outdir=config["outdir"]),
        annot       = config["GENOME_ANNOT"],
        ref         = config["GENOME"],
        regions     = config["REGIONS"],
        inpath      = "{outdir}/variant".format(outdir=config["outdir"]),
        sampleslist = lambda wildcards: expand(expand("{sample}",sample=samples['SampleName'])),
        bind        = config["BIND"],
        allmine_bin = config["allmine_bin"]
    shell:
        """
        singularity exec {params.bind} {params.allmine_bin} \
        R -e \
        "rmarkdown::render('./modules/markdown_gen.Rmd',params=list(asf='{params.sf}', awd='{params.wd}',aannot='{params.annot}',aref='{params.ref}', abed ='{params.regions}' ,asamples='{params.sampleslist}',asnps='{input.snps}',acov='{input.cov}'), output_file='{output.Rmd}')"
        rm -rf Run_report_files
        """

#####################################################################################
######################### end allmine ###############################################
