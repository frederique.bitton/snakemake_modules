__author__ = "INRAE GAFL"
__license__ = "MIT"
__copyright__ = "INRAE, GAFL 2020"


#SNP
#varscan on each bam
rule varscan:
    input:
        bam = "{outdir}/mapped/{{sample}}_sorted_parsed.bam".format(outdir=config["outdir"])
        #on merged bams
        #bam = "{outdir}/bams_merged_sorted.bam".format(outdir=config["outdir"])
    output:
        var = "{outdir}/variant/{{sample}}_varscan.vcf".format(outdir=config["outdir"])
        #on merged bams
        #var = "{outdir}/variant/SNPs4all_freebayes.vcf".format(outdir=config["outdir"])
    params:
        ref = config["REFPATH"] + "/" + config["GENOME"],
        bind = config["BIND"],
        samtools_bin = config["samtools_bin"],
        varscan_bin  = config["varscan_bin"],
        freebayes_bin = config["freebayes_bin"]
    message: "Looking for SNP in {input.bam} with Varscan \n"
    #from mpileup to varscan to save disk space
    shell:
        """
        zero=$(singularity exec {params.bind} {params.samtools_bin} samtools flagstat {input.bam}|head -n1|sed 's/\s.*//g')
        if [ "$zero" -lt "1" ]
        then
                echo "WARNING ALLMINE {output.var} EMPTY"
                touch {output.var}
                exit 0
        fi
        singularity exec {params.bind} {params.samtools_bin} \
        samtools mpileup \
        -C 50 \
        -A \
        -d 50000 \
        -f {params.ref} \
        {input.bam} | \
        singularity exec {params.bind} {params.varscan_bin} \
        varscan mpileup2snp \
        --p-value 0.99 \
        --min-coverage 8 \
        --output-vcf 1 \
        --min-strands2 1 \
        --min-var-freq 0.15 \
        --min-freq-for-hom 0.75 \
        --min-avg-qual 20 \
        > {output.var}
        """
