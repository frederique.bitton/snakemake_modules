__author__ = "INRAE GAFL"
__license__ = "MIT"
__copyright__ = "INRAE, GAFL 2020"

#rule to merge all bams files
rule merge_bams :
    input:
        bams=lambda wildcards: expand(expand("{outdir}/mapped/{sample}_sorted.bam", outdir=config["outdir"], sample=samples['SampleName'])),
        bamslist = "{outdir}/bams.list".format(outdir=config["outdir"])
    output:
        bam = "{outdir}/bams_merged_sorted.bam".format(outdir=config["outdir"])
    params:
        bamsp       = "{outdir}/mapped".format(outdir=config["outdir"]),
        outlist     = config["outdir"],
        samtools_bin = config["samtools_bin"],
        bind = config["BIND"]
    threads: 10
    shell:"""
        singularity exec {params.bind} {params.samtools_bin} samtools merge -@{threads} -f -b {input.bamslist} {output.bam}
        singularity exec {params.bind} {params.samtools_bin} samtools index -@{threads} {output.bam}
        """
